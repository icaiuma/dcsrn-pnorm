from __future__ import print_function, division, absolute_import, unicode_literals
from tf_dcsrn import dcsrnPnorm, image_util, util, patches_3d
from ssim import ssim
from FFT import LRbyFFT

import os
import shutil
import numpy as np
import logging
from scipy import io
import glob
import nibabel
import cv2

import tensorflow as tf
from keras import backend as k

###################################
os.environ["CUDA_VISIBLE_DEVICES"]="1" #To force tensorflow to only see one GPU.
# TensorFlow wizardry
config = tf.ConfigProto()
 
# Don't pre-allocate memory; allocate as-needed
#config.gpu_options.allow_growth = True
 
# Only allow a total of half the GPU memory to be allocated
#config.gpu_options.per_process_gpu_memory_fraction = 0.5
 
# Create a session with the above options specified.
sess = tf.Session(config=config)
k.tensorflow_backend.set_session(sess)
###################################

p = 2.0
q = 1.7

models = ["/WCS_p"+str(p)+"q"+str(q)+"/model_WCS_"+str(p)+"_"+str(q)+".cpkt","/WSS_p"+str(p)+"q"+str(q)+"/model_WSS_"+str(p)+"_"+str(q)+".cpkt","/p"+str(1.7)+"/model_"+str(1.7)+".cpkt","/p"+str(2.0)+"/model_"+str(2.0)+".cpkt"]
model_name = ['WCS','WSS','1.7','2.0']

for k in range(4):

    output_path = "./snapshots/Runs/train"+str(4)
    # path of dataset, here is the HCP dataset
    dataset_HCP = "/usr/share/Data2/ICAE/TestImages/test"+str(4)

    checkpoint_path = output_path+models[k]
    if k == 0:
        net = dcsrnPnorm.DCSRN_WCS(channels=1,pnorms=(p,q),weights=(1.0,0.45))
    if k == 1:
        net = dcsrnPnorm.DCSRN_WSS(channels=1,pnorms=(p,q),weights=(0.65,0.35))
    if k == 2:
        net = dcsrnPnorm.DCSRN(channels=1,pnorm=1.7)
    if k == 3:
        net = dcsrnPnorm.DCSRN(channels=1,pnorm=2.0)

    #net.restore(sess,checkpoint_path)


    # Testing to reconstruct an image

    files = glob.glob(dataset_HCP+"/*.nii")
    thisimage = nibabel.load(files[4]).get_data().transpose(1, 0, 2)

    thisimage = np.array(thisimage, dtype = np.float32)
    thisimage -= np.amin(thisimage)
    thisimage /= np.amax(thisimage)
    #io.savemat('./HRimage.mat', mdict={'data': thisimage})

    imshape = thisimage.shape
    imstride = 32
    padding = np.zeros(3,int)
    for i in range(0,3):
        if not imshape[i] % imstride == 0:
            padding[i] = (np.remainder(((imshape[i]//imstride)+1)*imstride,imshape[i])/float(2)).astype(int)
    image_pad = np.squeeze(thisimage)
    image_pad = np.pad(thisimage,((padding[0],),(padding[1],),(padding[2],)))

    patches = patches_3d.extract_patches_3d(image_pad,(64,64,64),stride=imstride)

    if k == 0:
        io.savemat('./HRpatches.mat', mdict={'HRp': patches})
    #cv2.imshow('HR',patches[100,:,:,32])

    lr_patches = np.empty_like(patches)
    for i in range(patches.shape[0]):
        thispatch = patches[i,]
        lr_patches[i,] = LRbyFFT.getLR(thispatch)

    if k == 0:
        io.savemat('./LRpatches.mat', mdict={'LRp': lr_patches})

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        net.restore(sess,checkpoint_path)
        SRpatches = np.empty(patches.shape)
        ssim_patches = np.zeros(patches.shape[0]//5)
        for n in range(0,patches.shape[0]//5):
            patch_result = sess.run(net.logits, feed_dict={net.x: lr_patches[n*5:(n+1)*5,]})
            SRpatches[n*5:(n+1)*5,] = patch_result
            #ssim_patches[n] = tf.reduce_mean(net._get_ssim(tf.convert_to_tensor(patch_result),tf.convert_to_tensor(patches[n*4:(n+1)*4,],tf.float32))).eval()

    io.savemat('./SRpatches'+model_name[k]+'.mat', mdict={'SRp': SRpatches})
    #cv2.imshow(model_name[k],SRpatches[100,:,:,32])

    #print("Mean of patches= "+str(np.mean(ssim_patches)))
    #print("Mean of patches= "+str(np.mean(np.clip(ssim_patches,0,1))))

    image_r = np.array(patches_3d.reconstruct_from_patches_3d(SRpatches,image_pad.shape,stride=imstride),dtype=np.float32)
    image_rt = image_r[padding[0]:image_pad.shape[0]-padding[0],padding[1]:image_pad.shape[1]-padding[1],padding[2]:image_pad.shape[2]-padding[2]]

    #print((image_rt< 0.).sum())
    image_rt = np.clip(image_rt,0,1)
    #image_rt -= np.amin(image_rt)
    #image_rt /= np.amax(image_rt)

    '''
    with tf.Session() as sess:
        print("Testing SSIM= "+str(tf.reduce_mean(net._get_ssim(tf.convert_to_tensor(image_rt,tf.float32),tf.convert_to_tensor(thisimage,tf.float32))).eval()))
    print("Testing MSE= "+str(np.sum(np.square(thisimage - image_rt))))

    io.savemat('./SRimage.mat', mdict={'data': image_rt})
    '''


