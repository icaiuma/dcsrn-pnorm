from __future__ import print_function, division, absolute_import, unicode_literals

import os
import shutil
import numpy as np
import logging

import tensorflow as tf
from keras import backend as k

###################################
os.environ["CUDA_VISIBLE_DEVICES"]="1" #To force tensorflow to only see one GPU.
# TensorFlow wizardry
config = tf.ConfigProto()
 
# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth = True
 
# Only allow a total of half the GPU memory to be allocated
#config.gpu_options.per_process_gpu_memory_fraction = 0.5
 
# Create a session with the above options specified.
k.tensorflow_backend.set_session(tf.Session(config=config))
###################################

from tf_dcsrn import dcsrnPnorm, image_util

bestPnorm = 2.0
weightsNorms = (1.0,0.75)
for q in np.around(np.arange(1.5,2,0.1,dtype=np.float32),2):

    output_path = "./snapshots/WCSmodel_p"+str(bestPnorm)+"_q"+str(q)+"/"
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    # path of dataset, here is the HCP dataset
    dataset_HCP = "./HCP_NPY_Augment/"

    #preparing data loading, you may want to explicitly note the glob search path on you data 
    data_provider = image_util.MedicalImageDataProvider(search_path=dataset_HCP)

    print("\ndata_provider initialization over.\n")

    # setup & training
    net = dcsrnPnorm.DCSRN_WCS(channels=1,pnorms=(bestPnorm,q),weights=weightsNorms)

    print("\nGraph set over.\n")
    print("\nTraining with p-norm = "+str(bestPnorm)+" q-norm = "+str(q)+" and weights "+str(weightsNorms)+"\n")

    trainer = dcsrnPnorm.Trainer(net,opt_kwargs={'learning_rate': 0.00001})

    print("\nBegin to train.\n")

    path = trainer.train(data_provider, output_path, model_name="model_p"+str(bestPnorm)+"_q"+str(q)+".cpkt", restore = True)

    print("\nTraining process is over.\n")

