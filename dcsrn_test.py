from __future__ import print_function, division, absolute_import, unicode_literals
from tf_dcsrn import dcsrnPnorm, image_util, util, patches_3d
from ssim import ssim
from FFT import LRbyFFT

import os
import shutil
import numpy as np
import logging
from scipy import io
import glob
import nibabel

import tensorflow as tf
from keras import backend as k

###################################
os.environ["CUDA_VISIBLE_DEVICES"]="1" #To force tensorflow to only see one GPU.
# TensorFlow wizardry
config = tf.ConfigProto()
 
# Don't pre-allocate memory; allocate as-needed
#config.gpu_options.allow_growth = True
 
# Only allow a total of half the GPU memory to be allocated
#config.gpu_options.per_process_gpu_memory_fraction = 0.5
 
# Create a session with the above options specified.
sess = tf.Session(config=config)
k.tensorflow_backend.set_session(sess)
###################################

pnorm = 2.0

output_path = "./snapshots/model_p"+str(pnorm)+"/"
# path of dataset, here is the HCP dataset
dataset_HCP = "./HCP_NPY_Augment/"

checkpoint_path = output_path+"model_"+str(pnorm)+".cpkt"
net = dcsrnPnorm.DCSRN(channels=1,pnorm=pnorm)
#net.restore(sess,checkpoint_path)

# verification, randomly test 4 images
test_provider = image_util.MedicalImageDataProvider(search_path=dataset_HCP)
test_x, test_y = test_provider(4)

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    net.restore(sess,checkpoint_path)
    result = sess.run(net.logits, feed_dict={net.x: test_x})

    print("Av Testing SSIM= "+str(tf.reduce_mean(net._get_ssim(tf.convert_to_tensor(result),tf.convert_to_tensor(test_y,tf.float32)),-1).eval()))
    print("Av Testing SSIM= "+str(tf.reduce_mean(tf.reduce_mean(net._get_ssim(tf.convert_to_tensor(result),tf.convert_to_tensor(test_y,tf.float32)),-1)).eval()))
        
    for ndx in range(result.shape[0]):

        #print("Testing SSIM= "+str(net._get_ssim(result[ndx,:,:,:,:],tf.cast(test_y[ndx,:,:,:,:],tf.float32)).eval()))
        print("Testing SSIM= "+str(tf.reduce_mean(net._get_ssim(tf.convert_to_tensor(result[ndx,:,:,:,:]),tf.convert_to_tensor(test_y[ndx,:,:,:,:],tf.float32))).eval()))
        
        filename = "./SRresult"+str(ndx)+".mat"
        io.savemat(filename,{"im": result[ndx,:,:,:,:]})
        filename = "./HRresult"+str(ndx)+".mat"
        io.savemat(filename,{"im": test_y[ndx,:,:,:,:]})
        
'''
maxim = 0
minim = 0
for filepath in glob.glob('./HCP_NPY/*.npy'):
    im = np.array(np.load(filepath),dtype=np.float32)
    maxim = max(np.amax(im),maxim)
    minim = min(np.amin(im),minim)
print(maxim) #666.0
print(minim) #-21.0
'''

# Testing to reconstruct an image

files = glob.glob("./HCP/*/*/*/*/*.nii")
for filepath in files:
    thisimage = nibabel.load(filepath).get_data().transpose(1, 0, 2)

thisimage = np.array(thisimage, dtype = np.float32)
#thisimage = np.fabs(thisimage)
thisimage -= np.amin(thisimage)
thisimage /= np.amax(thisimage)
#print((thisimage<0).sum())
io.savemat('./HRimage.mat', mdict={'data': thisimage})

imshape = thisimage.shape
imstride = 32
padding = np.zeros(3,int)
for i in range(0,3):
    if not imshape[i] % imstride == 0:
        padding[i] = (np.remainder(((imshape[i]//imstride)+1)*imstride,imshape[i])/float(2)).astype(int)
image_pad = np.squeeze(thisimage)
image_pad = np.pad(thisimage,((padding[0],),(padding[1],),(padding[2],)))

print(image_pad.shape)

#lr_image = LRbyFFT.getLR(image_pad)
#result_p = net.predict(checkpoint_path, lr_image)
#print(np.sum(np.square(image_pad - lr_image)))
#io.savemat('./LRimage.mat', mdict={'data': lr_image})

patches = patches_3d.extract_patches_3d(image_pad,(64,64,64),stride=imstride)
lr_patches = np.empty_like(patches)
for i in range(patches.shape[0]):
    thispatch = patches[i,]
    lr_patches[i,] = LRbyFFT.getLR(thispatch)

print(lr_patches.shape)

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    net.restore(sess,checkpoint_path)
    SRpatches = np.empty(patches.shape)
    ssim_patches = np.zeros(patches.shape[0]//5)
    for n in range(0,patches.shape[0]//5):
        patch_result = sess.run(net.logits, feed_dict={net.x: lr_patches[n*5:(n+1)*5,]})
        SRpatches[n*5:(n+1)*5,] = patch_result
        #ssim_patches[n] = tf.reduce_mean(net._get_ssim(tf.convert_to_tensor(patch_result),tf.convert_to_tensor(patches[n*4:(n+1)*4,],tf.float32))).eval()

print(SRpatches.shape)

#print("Mean of patches= "+str(np.mean(ssim_patches)))
#print("Mean of patches= "+str(np.mean(np.clip(ssim_patches,0,1))))

image_r = np.array(patches_3d.reconstruct_from_patches_3d(SRpatches,image_pad.shape,stride=imstride),dtype=np.float32)
image_rt = image_r[padding[0]:image_pad.shape[0]-padding[0],padding[1]:image_pad.shape[1]-padding[1],padding[2]:image_pad.shape[2]-padding[2]]

#print((image_rt< 0.).sum())
image_rt = np.clip(image_rt,0,1)
#image_rt -= np.amin(image_rt)
#image_rt /= np.amax(image_rt)

with tf.Session() as sess:
    print("Testing SSIM= "+str(tf.reduce_mean(net._get_ssim(tf.convert_to_tensor(image_rt,tf.float32),tf.convert_to_tensor(thisimage,tf.float32))).eval()))
print("Testing MSE= "+str(np.sum(np.square(thisimage - image_rt))))

io.savemat('./SRimage.mat', mdict={'data': image_rt})


'''
# Generate 10 fake images, last dimension can be different than 3
imshape = np.array([256, 240, 240])
imstride = 32
images = np.random.random((10, imshape[0], imshape[1], imshape[2], 1)).astype(np.float32)

#padding = int(np.abs(PatchSize - LabelSize)/float(2))
padding = np.zeros(3,int)
for i in range(0,3):
    if not imshape[i] % imstride == 0:
        padding[i] = (np.remainder(((imshape[i]//imstride)+1)*imstride,imshape[i])/float(2)).astype(int)

image_or = np.squeeze(images[1,])
image_1 = np.pad(image_or,((padding[0],),(padding[1],),(padding[2],)))
print(image_1.shape)

patches = patches_3d.extract_patches_3d(image_1,(64,64,64),stride=imstride)
print(patches.shape)
image_r = patches_3d.reconstruct_from_patches_3d(patches,image_1.shape,stride=imstride)
print(np.sum(np.square(image_1 - image_r)))

image_rt = image_r[padding[0]:image_r.shape[0]-padding[0],padding[1]:image_r.shape[1]-padding[1],padding[2]:image_r.shape[2]-padding[2]]
print(np.sum(np.square(image_or - image_rt)))
'''

# Extract patches
# patch_size = 64
# _,n_row,n_col,n_deep,n_channel = images.shape
# n_patch = n_row*n_col*n_deep // (patch_size**3) #assume square patch

# patches = extract_patches(images)
# patches = tf.reshape(patches,[n_patch,patch_size,patch_size,patch_size,n_channel])

# rows = tf.split(patches,n_col//patch_size,axis=0)
# rows = [tf.concat(tf.unstack(x),axis=1) for x in rows] 

# reconstructed = tf.concat(rows,axis=0)
# Reconstruct image
# Notice that original images are only passed to infer the right shape
# images_reconstructed = extract_patches_inverse(images, patches)

# with tf.Session() as sess:
#     images_r = images_reconstructed.eval(session=sess)

# print(np.isnan(images_r[1,]))
# print(np.sum(np.square(images - images_r))) 


