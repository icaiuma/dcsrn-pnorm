from __future__ import print_function, division, absolute_import, unicode_literals

import os
import shutil
import numpy as np
import logging

import tensorflow as tf
from keras import backend as k

###################################
os.environ["CUDA_VISIBLE_DEVICES"]="0" #To force tensorflow to only see one GPU.
# TensorFlow wizardry
config = tf.ConfigProto()
 
# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth = True
 
# Only allow a total of half the GPU memory to be allocated
config.gpu_options.per_process_gpu_memory_fraction = 0.5
 
# Create a session with the above options specified.
k.tensorflow_backend.set_session(tf.Session(config=config))
###################################

from tf_dcsrn import dcsrn, image_util

output_path = "./snapshots/"
# path of dataset, here is the HCP dataset
dataset_HCP = "./HCP_NPY_Augment/"

#preparing data loading, you may want to explicitly note the glob search path on you data 
data_provider = image_util.MedicalImageDataProvider()

print("\ndata_provider initialization over.\n")

# setup & training
net = dcsrn.DCSRN(channels=1)

print("\nGraph set over.\n")

trainer = dcsrn.Trainer(net)

print("\nBegin to train.\n")

path = trainer.train(data_provider, output_path, restore = True)

print("\nTraining process is over.\n")

# verification, randomly test 4 images
test_provider = image_util.MedicalImageDataProvider()
test_x, test_y = test_provider(4)
result = net.predict(path, test_x)
