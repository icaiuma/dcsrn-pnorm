% Script to compute stats for DCSRN
% matlab -nojvm -nodisplay -nosplash -r "run('ComputeStatsRepetitionsDCSRN.m'); exit();"

dataPath = '/home/karlkhader/Documents/deepBrain-master/ICAE/TestImages/DCSRN';

savePath = './ResultsDCSRN';

netType = 'WCS_p2.0q1.9';
% netType = 'WSS_p2.0q1.7';
% netType = 'p1.7';
% netType = 'p2.0';


maxValue = 4095;

% maxNd = 1;

% for zoom = [2 3 4]
% 
%     zoomPath = fullfile(savePath,['Zoom',num2str(zoom)]);
%     mkdir(zoomPath)
%     fullfile(zoomPath,'GTimages')
    
for i = 1:10

    testPath = fullfile(savePath,['test',num2str(i)]);
    mkdir(testPath)

    images = dir(fullfile(dataPath,['test',num2str(i)],netType,'*.mat'));

    % Results file
    ResultsFileName = fullfile(testPath,[netType,'_test',num2str(i),'_Results.mat']);
    Results = struct([]);

    for j = 1:numel(images)

        thisImage = images(j).name(1:end-4);
        fprintf('\nProcessing %s\n',thisImage);
        ThisImageLabel =  images(j).name(1:6);

        data = load(fullfile(images(j).folder,images(j).name));
        HR = double(data.HR);
        RestoredImage = double(data.SR);
        % originalfile = fullfile(dataPath,['test',num2str(i)],[thisImage,'.nii']);
        % HR = double(niftiread(originalfile));
        % HR = HR/maxValue;

        % V = double(load(fullfile(images(j).folder,images(j).name)));
        % RestoredImage = V/maxValue;
        

        Quality=RestorationQuality(255*HR,255*RestoredImage);
        Results(j).MSE=Quality.MSE;
        Results(j).BrainMSE=Quality.BrainMSE;
        Results(j).BrainMSE10=Quality.BrainMSE10;
        Results(j).RMSE=Quality.RMSE;
        Results(j).PSNR=Quality.PSNR;
        Results(j).BrainPSNR=Quality.BrainPSNR;
        Results(j).BrainPSNR10=Quality.BrainPSNR10;
        Results(j).SNR=Quality.SNR;
        Results(j).BrainSNR10=Quality.BrainSNR10;
        Results(j).SSIM3d=Quality.SSIM3d;
        Results(j).SSIM3d10=Quality.SSIM3d10;
        Results(j).SSIMor=Quality.SSIMor;
        Results(j).MatlabSSIM=Quality.MatlabSSIM;
        Results(j).BrainMatlabSSIM=Quality.BrainMatlabSSIM;
        Results(j).BrainMatlabSSIM10=Quality.BrainMatlabSSIM10;
        Results(j).BC=Quality.BC;
        Results(j).BrainBC=Quality.BrainBC;
        Results(j).BrainBC10=Quality.BrainBC10;

        fprintf('Results for image %s and net = %s:\n',...
            thisImage,netType);
        fprintf('PSNR: %d\n',Quality.PSNR);
        fprintf('SSIM: %d\n',Quality.MatlabSSIM);
        fprintf('BC: %d\n',Quality.BC);

        RestoredImage_uint8=uint8(RestoredImage*255);

        Results(j).RestoredImage=RestoredImage_uint8;
        Results(j).Name = ThisImageLabel;
        save(ResultsFileName,'Results','netType');

    end

end
    
% end

