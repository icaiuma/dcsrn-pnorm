from __future__ import print_function, division, absolute_import, unicode_literals
from tf_dcsrn import dcsrnPnorm, image_util, util, patches_3d
from ssim import ssim
from FFT import LRbyFFT

import os
import shutil
import numpy as np
import logging
from scipy import io
import glob
import nibabel

import tensorflow as tf
from keras import backend as k

###################################
os.environ["CUDA_VISIBLE_DEVICES"]="0" #To force tensorflow to only see one GPU.
# TensorFlow wizardry
config = tf.ConfigProto()
 
# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth = True
 
# Only allow a total of half the GPU memory to be allocated
config.gpu_options.per_process_gpu_memory_fraction = 0.5
 
# Create a session with the above options specified.
sess = tf.Session(config=config)
k.tensorflow_backend.set_session(sess)
###################################

p = 2.0
q = 1.7
weightsNorms = (0.65,0.35)
for i in range(1,11):

    output_path = "/home/karlkhader/Documents/deepBrain-master/ICAE/TestImages/DCSRN/test"+str(i)+"/WSS_p"+str(p)+"q"+str(q)+"/"
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    model_path = "./snapshots/Runs/train"+str(i)+"/WSS_p"+str(p)+"q"+str(q)+"/"

    checkpoint_path = model_path+"model_WSS_"+str(p)+"_"+str(q)+".cpkt"
    net = dcsrnPnorm.DCSRN_WSS(channels=1,pnorms=(p,q),weights=weightsNorms)

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        net.restore(sess,checkpoint_path)

        print("Testing test"+str(i)+"\n")

        # Testing to reconstruct an image

        files = glob.glob("/usr/share/Data2/ICAE/TestImages/test"+str(i)+"/*.nii")
        for filepath in files:
            print("Testing image "+os.path.basename(filepath)[:-4]+"\n")

            hr_image = nibabel.load(filepath).get_data().transpose(1, 0, 2)

            hr_image = np.array(hr_image, dtype = np.float32)
            #hr_image = np.fabs(hr_image)
            hr_image -= np.amin(hr_image)
            hr_image /= np.amax(hr_image)

            imshape = hr_image.shape
            imstride = 32
            padding = np.zeros(3,int)
            for j in range(0,3):
                if not imshape[j] % imstride == 0:
                    padding[j] = (np.remainder(((imshape[j]//imstride)+1)*imstride,imshape[j])/float(2)).astype(int)
            image_pad = np.pad(hr_image,((padding[0],),(padding[1],),(padding[2],)))

            patches = patches_3d.extract_patches_3d(image_pad,(64,64,64),stride=imstride)
            lr_patches = np.empty_like(patches)
            for j in range(patches.shape[0]):
                lr_patches[j,] = LRbyFFT.getLR(patches[j,])


            test_bs = 7 #567 patches in total, divisible by 7
            SRpatches = np.empty(patches.shape)
            for n in range(0,patches.shape[0]//test_bs):
                SRpatches[n*test_bs:(n+1)*test_bs,] = sess.run(net.logits, feed_dict={net.x: lr_patches[n*test_bs:(n+1)*test_bs,]})


            image_r = np.array(patches_3d.reconstruct_from_patches_3d(SRpatches,image_pad.shape,stride=imstride),dtype=np.float32)
            sr_image = image_r[padding[0]:image_pad.shape[0]-padding[0],padding[1]:image_pad.shape[1]-padding[1],padding[2]:image_pad.shape[2]-padding[2]]

            sr_image = np.clip(sr_image,0,1)
            #sr_image -= np.amin(sr_image)
            #sr_image /= np.amax(sr_image)
            '''
            with tf.Session() as sess:
                print("Testing SSIM= "+str(tf.reduce_mean(net._get_ssim(tf.convert_to_tensor(sr_image,tf.float32),tf.convert_to_tensor(hr_image,tf.float32))).eval()))
            print("Testing MSE= "+str(np.sum(np.square(hr_image - sr_image))))
            '''
            io.savemat(output_path+os.path.basename(filepath)[:-4]+'.mat', mdict={'HR': hr_image, 'SR': sr_image})



