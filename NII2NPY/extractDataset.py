import numpy as np
import os
import glob
from PIL import Image
import sys
import zipfile
from sh import gunzip
import shutil
import time

sys.path.append('../')

  
datapath = '/usr/share/Data2/HCP1113_Dataset/'
outputpath = '/usr/share/Data2/HCP1113_Dataset_nii/'

'''
with zipfile.ZipFile(datapath, 'r') as zip_ref:
    zip_ref.extractall(datapath)
'''

filepaths = glob.glob('/usr/share/Data2/HCP1113_Dataset/*.zip')

for file in filepaths:
    filename = os.path.basename(file)
    key = filename[:6]
    extractfile = key+'/unprocessed/3T/T1w_MPR1/'+key+'_3T_T1w_MPR1.nii.gz'
    with zipfile.ZipFile(file) as zip:
        zip_info = zip.getinfo(extractfile)
        zip_info.filename = key+'_3T_T1w_MPR1.nii.gz'
        zip.extract(zip_info,outputpath)

    gunzip(outputpath+key+'_3T_T1w_MPR1.nii.gz')

    print("Extracted file " + str(key))


