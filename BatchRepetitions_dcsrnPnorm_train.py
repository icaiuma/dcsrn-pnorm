from __future__ import print_function, division, absolute_import, unicode_literals

import os
import shutil
import numpy as np
import logging

import tensorflow as tf
from keras import backend as k

###################################
os.environ["CUDA_VISIBLE_DEVICES"]="1" #To force tensorflow to only see one GPU.
# TensorFlow wizardry
config = tf.ConfigProto()
 
# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth = True
 
# Only allow a total of half the GPU memory to be allocated
#config.gpu_options.per_process_gpu_memory_fraction = 0.5
 
# Create a session with the above options specified.
k.tensorflow_backend.set_session(tf.Session(config=config))
###################################

from tf_dcsrn import dcsrnPnorm, image_util

p = 1.7
for i in range(1,11):

    output_path = "./snapshots/Runs/train"+str(i)+"/p"+str(p)+"/"
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    # path of dataset, here is the HCP dataset
    dataset_HCP = "/usr/share/Data2/ICAE/TrainImages/HCP_NPY_Augment/train"+str(i)+"/"

    #preparing data loading, you may want to explicitly note the glob search path on you data 
    data_provider = image_util.MedicalImageDataProvider(search_path=dataset_HCP,a_min=0.,a_max=4095.,shuffle_data=False)

    print("\ndata_provider initialization over.\n")

    # setup & training
    net = dcsrnPnorm.DCSRN(channels=1,pnorm=p)

    print("\nGraph set over.\n")
    print("\nTraining train"+str(i)+" with p-norm = "+str(p)+"\n")

    trainer = dcsrnPnorm.Trainer(net)

    print("\nBegin to train.\n")

    path = trainer.train(data_provider, output_path, model_name="model_"+str(p)+".cpkt", restore = True, epochs=14)

    print("\nTraining process is over.\n")

