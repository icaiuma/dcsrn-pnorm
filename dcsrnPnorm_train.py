from __future__ import print_function, division, absolute_import, unicode_literals

import os
import shutil
import numpy as np
import logging

import tensorflow as tf
from keras import backend as k

###################################
os.environ["CUDA_VISIBLE_DEVICES"]="1" #To force tensorflow to only see one GPU.
# TensorFlow wizardry
config = tf.ConfigProto()
 
# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth = True
 
# Only allow a total of half the GPU memory to be allocated
#config.gpu_options.per_process_gpu_memory_fraction = 0.5
 
# Create a session with the above options specified.
k.tensorflow_backend.set_session(tf.Session(config=config))
###################################

from tf_dcsrn import dcsrnPnorm, image_util

output_path = "./snapshots/model_p2.0/"
# path of dataset, here is the HCP dataset
dataset_HCP = "./HCP_NPY_Augment/"

#preparing data loading, you may want to explicitly note the glob search path on you data 
data_provider = image_util.MedicalImageDataProvider(search_path=dataset_HCP)

print("\ndata_provider initialization over.\n")

# setup & training
net = dcsrnPnorm.DCSRN(channels=1,pnorm=2.0)

print("\nGraph set over.\n")

trainer = dcsrnPnorm.Trainer(net)

print("\nBegin to train.\n")

path = trainer.train(data_provider, output_path, model_name="model_2.0.cpkt", restore = True)

print("\nTraining process is over.\n")

# verification, randomly test 4 images
test_provider = image_util.MedicalImageDataProvider()
test_x, test_y = test_provider(4)
result = net.predict(path, test_x)
