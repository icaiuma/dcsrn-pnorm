function [Results]=RestorationQuality(OrigImg,RestoredImg)
% Computes restoration quality measures for images in the [0,255] range, as in:
% C.S. Anand, J.S. Sahambi / Magnetic Resonance Imaging 28 (2010) 842�861

ind = OrigImg>0;
ind10 = OrigImg>10;

PlainErrors=(OrigImg-RestoredImg);
BrainPlainErrors=(OrigImg(ind)-RestoredImg(ind));
BrainPlainErrors10=(OrigImg(ind10)-RestoredImg(ind10));

% Mean Squared Error (MSE)
Results.MSE=mean(PlainErrors(:).^2);
Results.BrainMSE=mean(BrainPlainErrors(:).^2);
Results.BrainMSE10=mean(BrainPlainErrors10(:).^2);

% Root Mean Squared Error (RMSE)
Results.RMSE=sqrt(Results.MSE);

% Power Signal-to-Noise Ratio (PSNR)
Results.PSNR=10*log10((255^2)/Results.MSE);
Results.BrainPSNR=10*log10((255^2)/Results.BrainMSE);
Results.BrainPSNR10=10*log10((255^2)/Results.BrainMSE10);

% Signal-to-Noise Ratio (SNR)
Results.SNR = 10*log10(mean(OrigImg(:).^2)/Results.MSE);
Results.BrainSNR10 = 10*log10(mean(OrigImg(ind10).^2)/Results.BrainMSE10);

% Structural Similarity Index (SSIM)
Results.SSIM3d=ssim_index3d(OrigImg,RestoredImg,[1 1 1],ind);
Results.SSIM3d10=ssim_index3d(OrigImg,RestoredImg,[1 1 1],ind10);

for ZSlice=1:size(OrigImg,3)
    SSIM(ZSlice)=ssim_index(OrigImg(:,:,ZSlice),RestoredImg(:,:,ZSlice));
end
Results.SSIMor=mean(SSIM);

[ssimval, ssimmap] = ssim(OrigImg/255,RestoredImg/255);
Results.MatlabSSIM = ssimval;
Results.BrainMatlabSSIM = mean(ssimmap(ind));
Results.BrainMatlabSSIM10 = mean(ssimmap(ind10));

% Bhattacharrya coefficient (BC)
HistOrig=histc(OrigImg(:),0:256);
HistOrig=HistOrig/sum(HistOrig);
HistRestored=histc(RestoredImg(:),0:256);
HistRestored=HistRestored/sum(HistRestored);
Results.BC=sum(sqrt(HistOrig.*HistRestored));

HistOrig=histc(OrigImg(ind),0:256);
HistOrig=HistOrig/sum(HistOrig);
HistRestored=histc(RestoredImg(ind),0:256);
HistRestored=HistRestored/sum(HistRestored);
Results.BrainBC=sum(sqrt(HistOrig.*HistRestored));

HistOrig=histc(OrigImg(ind10),0:256);
HistOrig=HistOrig/sum(HistOrig);
HistRestored=histc(RestoredImg(ind10),0:256);
HistRestored=HistRestored/sum(HistRestored);
Results.BrainBC10=sum(sqrt(HistOrig.*HistRestored));


% % Quality Index based on Local Variance (QILV)
% Results.QILV=qilv(OrigImg,RestoredImg,0);
% Results.SSIM3d10=ssim_index3d(OrigImg,RestoredImg,[1 1 1],ind10);


